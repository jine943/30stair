#include<stdio.h>
#include<iostream>

#define QUEUE_SIZE	25
#define ERROR		0xffffffff

using namespace std;

struct Data
{
	int x;
	int y;
};

/*
class JQueue
{
private:
	int m_queue[5];
	char m_rear = 0;
	char m_front = 0;

public:
	JQueue();
	~JQueue();

	int JQueue::push(int p_data) {
		// m_rear 를 0~ 24 로 순환하며 증가시킴 
		// 대신 m_front 와 같으면 X
		if (m_front != (m_rear = (m_rear + 1) % 5)) {
			m_queue[m_rear] = p_data;
		}
		else return ERROR;
		return 1;
	}

	int JQueue::pop()
	{
		if (m_front != m_rear) {
			m_front = (m_front + 1) % 5;
			return m_queue[m_front];
		}
		else return NULL;
	}

	void show() 
	{
		if (m_rear > m_front) {
			for (int i = 1 + m_front; i <= m_rear; i++) {
				cout << m_queue[i];
			}
			cout << endl;
		} else if (m_rear < m_front) { // r       f
			for (int i = ((1 + m_front) % 5); i < 5; i++) {
				cout << m_queue[i];
			}
			for (int i = 0; i <= m_rear; i++) {
				cout << m_queue[i];
			}
			cout << endl;
		}
	}

};

JQueue::JQueue()
{

}
JQueue::~JQueue()
{

}
*/


class JQueue
{
private:
	Data *m_queue[QUEUE_SIZE];
	char m_rear = 0;
	char m_front = 0;

public:
	JQueue();
	~JQueue();
	int Push(Data *p_data);
	Data *Pop();
	char GetCount();
};

JQueue::JQueue()
{

}
JQueue::~JQueue()
{
	if (m_rear > m_front) {
		for (int i = 1 + m_front; i <= m_rear; i++) {
			delete m_queue[i];
		}
		cout << endl;
	} else if (m_rear < m_front) { // r       f
		for (int i = ((1 + m_front) % QUEUE_SIZE); i < QUEUE_SIZE; i++) {
			delete m_queue[i];
		}
		for (int i = 0; i <= m_rear; i++) {
			delete m_queue[i];
		}
	}

}

int JQueue::Push(Data *p_data) {
	// m_rear 를 0~ 24 로 순환하며 증가시킴 
	// 대신 m_front 와 같으면 X
	if (m_front != (m_rear = (m_rear + 1) % QUEUE_SIZE)) {
		m_queue[m_rear] = p_data;
	}
	else return ERROR;
	return 1;
}

Data *JQueue::Pop()
{
	if (m_front != m_rear) {
		m_front = (m_front + 1) % QUEUE_SIZE;
		return m_queue[m_front];
	}
	else return NULL;
}
char JQueue::GetCount()
{
	if (m_rear > m_front) return m_rear - m_front;
	else if (m_rear < m_front) return m_rear + (QUEUE_SIZE - 1 - m_front);
	else return 0;
}



class SafeManagerOfCity
{
private:
	Data m_lake;
	char **mp_map;
	int m_time_to_build; // Time to make dam (K)
	char m_array_size;

	JQueue m_water_path;
	int m_time;


public:
	SafeManagerOfCity() {
		mp_map = NULL;
	}
	~SafeManagerOfCity() {
		if (NULL != mp_map) {
			for (int i = 0; i < m_array_size; i++) delete[] mp_map[i];
			delete[] mp_map;
		}
	}
	void InitInfo() {
		// 배열의 크기를 입력받는다
		cin >> m_array_size;


		// 입력된 배열의 크기만큼 2차원 배열을 만든다
		mp_map = new char*[m_array_size];
		for (char i = 0; i < m_array_size; i++) {
			mp_map[i] = new char[m_array_size];
		}

		char temp;
		// 2차원 배열에 블록의 상태를 담는다 
		// 1 : 물이 들어옮 or 건물 
		// 0 : 안전함
		for (char y = 0; y < 5; y++)
		{
			for (char x = 0; x < 5; x++) {
				cin >> temp;
				// cout << mp_map[i][j];
				if (temp == '0') {
					mp_map[y][x] = 0;
					// cout << "(" << (int)y << "," << (int)x << ") " << mp_map[y][x] << endl;
				} else mp_map[y][x] = 1;
			}
			// cout << endl;
		}

		cin >> m_lake.x; // 호수의 좌표 입력
		cin >> m_lake.y;
		m_lake.x--; // 배열 좌표로 변환
		m_lake.y--;
		cin >> m_time_to_build;		// 댐이 만들어지는 시간

	}
	// return value 0: safe,  1 : unsafe   -1 : invalid index
	char CheckBlockState(int x, int y) {
		if (y >= 0 && y < m_array_size && x >=0 && x < m_array_size) {
			if (mp_map[y][x] == 0){
				return 0; 
			}
			else return 1;
		}
		else - 1;
	}
	
	// x, y 좌표의 지도를 잠긴 곳으로 업데이트
	void SetWaterSpot(int x, int y) {
		if (!CheckBlockState(x, y)) { // 잠기지 않은 상태였다면 잠긴 상태로 변경
			mp_map[y][x] = 1;
			// 새로운 source 에 넣기
			Data *p_new_block = new Data;
			p_new_block->x = x;
			p_new_block->y = y;
			m_water_path.Push(p_new_block);
		}
	}

	void ProcessNextWaterSpot()
	{
		Data *p_source_block;
		char source_count = m_water_path.GetCount();
		
		for (int i = 0; i < source_count; i++) {
			p_source_block = m_water_path.Pop();
			// UP side
			SetWaterSpot(p_source_block->x, p_source_block->y - 1);

			// Down side
			SetWaterSpot(p_source_block->x, p_source_block->y + 1);

			// Left side
			SetWaterSpot(p_source_block->x - 1, p_source_block->y);

			// Down side
			SetWaterSpot(p_source_block->x + 1, p_source_block->y);
			delete p_source_block;
		}
	}

	int CountDamToBuild()
	{
		char state = 0;
		Data *p_lake = new Data;
		p_lake->x = m_lake.x;
		p_lake->y = m_lake.y;

		m_water_path.Push(p_lake);

		for (m_time = 0; m_time < m_time_to_build; m_time++) ProcessNextWaterSpot();
				
		return m_water_path.GetCount();
	}

};


int main()
{
	/*
	dam 프로그램 방법론
	블럭의 상태를 담는 2차원 배열
	0 : 물에 잠기지 않음	1: 물에 잠기거나 건물이 있음

	2개의 큐가 있다.
	source 큐 : t 시에 물이 잠긴 지역
	dest 큐 : t + 1 시에 물이 이동하는 지역
	dest 큐는 다시 source 큐가 된다.

	큐에는 블럭(x행, y열)을 저장
	댐이 완성되는 시간 K 시간에 dest (물이 이동하는 목적지) 큐의 데이터 수를 찾으면 됨!
	*/
	// source 가 되는 큐
	// 아래
	int i = 2;
	while (i--) {
		SafeManagerOfCity safe_manager;
		safe_manager.InitInfo();
		cout << safe_manager.CountDamToBuild();
	}
}
