#include <stdio.h>
#include <iostream>

#define ERROR	0xff

using namespace std;

class Jstack
{
private:
	char m_stack[25];
	char m_top; // 마지막 데이터를 가리키는 포인터

public:
	Jstack();
	~Jstack();
	char push(char a_data);
	char pop();
};

Jstack::Jstack()
{
	m_top = -1;	// 데이터가 없다
}

Jstack::~Jstack()
{

}

char Jstack::push(char a_data)
{
	if(24 != m_top) m_stack[++m_top] = a_data;
	else return ERROR;
}

char Jstack::pop()
{
	if (-1 != m_top) return m_stack[m_top--];
	else return ERROR;
}

int main()
{
	// match 프로그램은 문자열의 괄호를 매치해주는 기능을 함
	// 문자열을 하나씩 입력받으면
	// ( 는 스택에 push 하고 
	// ) 는 스택에서 (와 pop 하여 match 한다.
	// 만일 스택이 empty 여서 매치할것이 없다면 not match를 출력한다

	char string[51];
	char index = 0;
	char target_char;
	char match_index; // '(' 짝의 인덱스
	Jstack stack;

	scanf("%s", string);

	while (target_char = string[index]) {
		if (target_char == '(') {
			// cout << target << " " << (int)index;
			stack.push(index);
		}
		else if (target_char == ')') {
			match_index = stack.pop();
			if (ERROR != match_index) {
				cout << (int)match_index << " " << (int)index << endl;
			}
			else cout << "not match" << endl;
		}
		index++;
	}
}