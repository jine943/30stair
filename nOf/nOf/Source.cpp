#include <stdio.h>
#include <iostream>

using namespace std;
// n! = 1*2*3* *n 에서 이 수가 가지는 소인수 m 의 개수 구하기

int main()
{
	// m 의 개수
	int m_count;
	int n, m, num, i = 3;

	while (i--) {

		cin >> n;
		cin >> m;

		m_count = 0;
		num = 0;

		for (int i = n; i/m >= 1; i = num) {
			num = i / m;
			m_count += num;
		}

		cout << m_count<< endl;
	}
	/*
	for (int i = 1; i < n; i++) {
		num = i;
		while ((num % m) == 0) {
			num /= m;
			m_count++;
		}
	}
	*/

	return 0;
}
