#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;


int num;

int sosu[100] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 0, };
int sosu_count = 11;

// 1 : 소수  0: 소수 아님
int IsSosu(int a_num) 
{
	for (int i = 0; i < sosu_count; i++) {
		if (a_num >= sosu[i] && (a_num % sosu[i]) == 0) {
			return 0;
		}
	}
	return 1;
}

// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31	
int main()
{
	double sqrt_num;
	int temp;
	int last_sosu_count;
	
	// 소수로 나누어지지 않을 때까지 나눔
	// 안나누어지면 다음소수!
	// 결과 값이 1이면 끝
	int test_count = 3;
	while (test_count--) {
		cin >> num;
		for (int i = 0; i < sosu_count; i++) {
			while (num >= sosu[i] && (num % sosu[i]) == 0) {
				num /= sosu[i];
				cout << sosu[i] << " ";
			}
			if (num == 1) break;
		}
		// 기존에 있는 소수로 나누어지지 않은 숫자라면
		if (num > 1) {
			// 현재 최대의 소수보다 큰 소수를 연산하여 추가
			sqrt_num = sqrt(num);
			last_sosu_count = sosu_count; // 이전의 최대 소수 index 를 저장
			
			// 2 <= sosu <= sqrt(num)범위에 있는 모든 소수로 num 을 나누어본다
			for (temp = sosu[last_sosu_count - 1]; temp < sqrt_num; temp++) {
				if (IsSosu(temp)) {
					sosu[sosu_count++] = temp;
				}
			}
			for (int i = last_sosu_count; i < sosu_count; i++) {
				while (num >= sosu[i] && (num % sosu[i]) == 0) {
					num /= sosu[i];
					cout << sosu[i] << " ";
				}
			}
			// 그래도 나누어 떨어지지 않으면 소수이다!
			if (num > sosu[sosu_count - 1]) {
				sosu[sosu_count++] = num;
				cout << num;
			}
		}
		cout << endl;
		num = 0;
	}
	return 0;
}